// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.


/**
 * Aqui certamente deveria ser feita uma validação para a data informada.
 * Porém, para fins do dev teste, deixarei apenas com a funcionalidade que foi pedida.
 *
 * @param {string} userDate
 * @returns {string}
 */
function formatDate(userDate) {
    let splittedDate = userDate.split('/');
    return splittedDate[2] + splittedDate[0] + splittedDate[1];
}

console.log(formatDate("12/31/2014"));
