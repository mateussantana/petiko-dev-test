// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {
  return obj[prop] != undefined ? delete obj[prop] : false;
}

// Test case
console.log(removeProperty({id: '123', nome: 'Petiko', to_remove: 'Property to remove'}, 'to_remove'));