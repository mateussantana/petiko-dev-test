<?php
/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

A classe LeagueTablr acompanha o score de cada jogador em uma liga. Depois de cada jogo, o score do jogador é salvo utilizanod a função recordResult.

O Rank de jogar na liga é calculado utilizando a seguinte lógica:

1- O jogador com a pontuação mais alta fica em primeiro lugar. O jogador com a pontuação mais baixa fica em último.
2- Se dois jogadores estiverem empatados, o jogador que jogou menos jogos é melhor posicionado.
3- Se dois jogadores estiverem empatados na pontuação e no número de jogos disputados, então o jogador que foi o primeiro na lista de jogadores é classificado mais alto.


Implemente a funação playerRank que retorna o jogador de uma posição escolhida do ranking.

Exemplo:

$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);


Todos os jogadores têm a mesma pontuação. No entanto, Arnold e Chris jogaram menos jogos do que Mike, e como Chris está acima de Arnold na lista de jogadores, ele está em primeiro lugar.

Portanto, o código acima deve exibir "Chris".


*/

class LeagueTable
{
	public function __construct($players)
    {
		$this->standings = array();
		foreach($players as $index => $p)
        {
			$this->standings[$p] = array
            (
                'index' => $index,
                'games_played' => 0, 
                'score' => 0
            );
        }
	}
		
	public function recordResult($player, $score)
    {
		$this->standings[$player]['games_played']++;
		$this->standings[$player]['score'] += $score;
	}

    /**
     * Implementação mais legível utilizando função anônima
     *
     * @todo Fazer uma trativa de erro para casos em que $rank < 0 ou $rank > que o tamanho de $this->standings
     *
     * @param int $rank Posição requerida
     * @return string O nome do jogador
     */
	public function playerRank($rank)
    {
        # Ref: https://www.php.net/manual/pt_BR/function.uasort.php
        uasort($this->standings, function($a, $b){
            // Caso tenha empate no 'score'
            if ($a['score'] == $b['score']) {
                // Caso o empate persista em 'games_played'
                if ($a['games_played'] == $b['games_played']) {
                    // queremos o menor index
                    return $b['index'] - $a['index'];
                } else {
                    // menor número de jogos jogados
                    return $b['games_played'] - $a['games_played'];
                }
            } else {
                // aqui queremos a maior pontuação
                return $a['score'] - $b['score'];
            }
        });

        // Ao final de uasort() teremos o array ordenado do menor para o maior. Como é um ranking de jogos, precisamos
        // inverter o array para termos a ordenação do maior (pontuação) para o menor.
        $this->standings = array_reverse($this->standings, true);
        return array_keys($this->standings)[$rank-1];
	}

    /**
     * Implementação curta utilizando operador ternário e arrow function (recurso do PHP 7.4+)
     *
     * @todo Fazer uma trativa de erro para casos em que $rank < 0 ou $rank > que o tamanho de $this->standings
     *
     * @param int $rank Posição requerida
     * @return string O nome do jogador
     */
    public function playerRankShortImplementation($rank)
    {
        uasort($this->standings, fn($a, $b) => $a['score']==$b['score'] ? $a['games_played']==$b['games_played'] ? $b['index'] - $a['index'] : $b['games_played']-$a['games_played'] : $a['score']-$b['score']);
        return array_keys($this->standings)[count($this->standings) - $rank];
    }
}
      
$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);