<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/




class Palindrome
{
    /**
     * Verifica se uma palavra é um palíndromo
     *
     * @param string $word
     * @return bool Retorna true se $word é um palíndromo e false caso contrário
     */
    public static function isPalindrome($word)
    {
        return strcasecmp($word, strrev($word)) == 0 ? true : false;
    }

    /**
     * Verifica se uma palavra é um palíndromo
     *
     * @todo Implementar um tratamento de exceção para $word em caso de null ou vazio
     *
     * @param string $word
     * @return bool Retorna true se $word é um palíndromo e false caso contrário
     */
    public static function isPalindromeAnotherImplementation($word)
    {
        $splitted = mb_str_split($word);
        $length = count($splitted);

        for ($letter = 0; $letter <= intval($length/2); $letter++) {
            if (strcasecmp($splitted[$letter], $splitted[$length-$letter-1]) != 0)
                return false;
        }

        return true;
    }
}

//echo Palindrome::isPalindrome('SocorramMeSubiNoOnibusEmMarrocos') . PHP_EOL;
echo Palindrome::isPalindrome('Deleveled');