<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

Postmon é uma API para consultar CEP e encomendas de maneira fácil.

Implemente uma função que recebe CEP e retorna todos os dados reltivos ao endereço correspondente.

Exemplo:

getAddressByCep('13566400') retorna:
{
	"bairro": "Vila Marina",
	"cidade": "São Carlos",
	"logradouro": "Rua Francisco Maricondi",
	"estado_info": {
	"area_km2": "248.221,996",
	"codigo_ibge": "35",
		"nome": "São Paulo"
	},
	"cep": "13566400",
	"cidade_info": {
		"area_km2": "1136,907",
		"codigo_ibge": "3548906"
	},
	"estado": "SP"
}



Documentação:
https://postmon.com.br/


*/

class CEP
{
    private const api_base_url = 'https://api.postmon.com.br/v1/cep/';

    /**
     * Busca os dados de um endereço pelo número do CEP
     *
     * @todo Refatorar utilizando curl ou um pacote como GuzzleHttp para ter um maior controle de erros, exceções e http code response
     *
     * @param string $cep o CEP a ser pesquisada
     * @return array|null Um array associativo contendo os dados do cep ou false em caso de falha
     */
    public static function getAddressByCep($cep)
    {
        $cep = str_replace('.', '', str_replace('-', '', $cep));
        $content = @file_get_contents(self::api_base_url . urlencode($cep));
        return $content ? json_decode($content, true) : null;
    }
}

var_dump(CEP::getAddressByCep('13.5664-00'));